﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.IO;

namespace Cloudette
{
    /// <summary>
    /// Interaction logic for SkyDriveLogin.xaml
    /// </summary>
    public partial class SkyDriveLogin : Window
    {
        public SkyDriveLogin()
        {
            InitializeComponent();
            setUpWindowDimensions();
            String url = "https://login.live.com/oauth20_authorize.srf?client_id=0000000048110B0F&scope=wl.signin%20wl.offline_access&response_type=code&redirect_uri=https://login.live.com/oauth20_desktop.srf";
            browser.Navigate(url);
            
            startListening();
        }

        private void setUpWindowDimensions()
        {
            this.Width = 400 * System.Windows.SystemParameters.PrimaryScreenWidth / 1280;
            this.Height = 600 * System.Windows.SystemParameters.PrimaryScreenHeight / 800;
            browser.Width = this.Width;
            browser.Height = this.Height;
        }

        public static String authCode = string.Empty;

        private async void startListening()
        {
            while (true)
            {
                    await Task.Delay(5000);
                    if (browser.Source.AbsoluteUri.Contains("code=") == true)
                    {
                        authCode = browser.Source.AbsoluteUri;
                        break;
                    }
            }
            filterOutAuthCode();
            obtainTokens();
            sendTokenToCloudette();
            MainWindow.skyDriveMarker = true;
            this.Close();
        }

        String response = string.Empty;
        private void obtainTokens()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://login.live.com/oauth20_token.srf?client_id=0000000048110B0F&redirect_uri=https://login.live.com/oauth20_desktop.srf&client_secret=WDbbFoJu74P9VSj0XUMFGT7c9Rbd0lTe&code="+authCode+"&grant_type=authorization_code");
           
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            Stream resStream = httpResponse.GetResponseStream();
            StreamReader reader = new StreamReader(resStream);
            response = reader.ReadToEnd();
            int index = response.IndexOf("access_token");
            response = response.Substring(index);
        }

        private async void sendTokenToCloudette()
        {
            Socket soc = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            System.Net.IPAddress ipAdd = System.Net.IPAddress.Parse("192.168.42.1");
            System.Net.IPEndPoint remoteEP = new System.Net.IPEndPoint(ipAdd, 6789);
            soc.Connect(remoteEP);
            await Task.Delay(2000);
            byte[] byData = System.Text.Encoding.ASCII.GetBytes(response);
            soc.Send(byData);
        }

        private void filterOutAuthCode()
        {
            int index1 = authCode.IndexOf("code=");
            int index2 = authCode.Length - 1;
            index1 += 5;
            authCode = authCode.Substring(index1, index2 - index1);
        }
    }
}
