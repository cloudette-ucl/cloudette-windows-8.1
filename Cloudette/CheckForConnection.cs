﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using System.Web;
using System.Net;

namespace Cloudette
{
    class CheckForConnection
    {
        public static Boolean checkConnectionToCloudette()
        {
            bool pingable = false;
            Ping ping = new Ping();
            try
            {
                PingReply reply = ping.Send("192.168.42.1");
                pingable = reply.Status == IPStatus.Success;
            }
            catch
            {

            }
            return pingable;
        }

        public static bool checkConnectionToInternet() 
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.google.com");
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK) return true;
                else return false;
            }
            catch
            {
                return false;
            }
        }


    }
}
