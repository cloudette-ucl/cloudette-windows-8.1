﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.IO;
using System.Net.Sockets;

namespace Cloudette
{
    
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            setWindowDimensions();
            setUpAndGo();
        }

        private void setUpAndGo()
        {
            leftButton.IsEnabled = false;
            rightButton.IsEnabled = false;
            state = "";
            setMessage1Up();
            setMessage2Up();
            setAnimationUp();
            setLogoDimensions();
            setListViewDimensions();
            loadingAnimationInner();
            loadingAnimationOuter();
            setResultDimensions();
            setPasskeyBoxUp();
            startChecks();
        }

        private void setPasskeyBoxUp()
        {
            passkeyEntry.Visibility = System.Windows.Visibility.Hidden;
            passkeyEntry.Opacity = 0;
            passkeyEntry.Width = SSIDs.Width;
            passkeyEntry.Height = this.Height / 5;
            passkeyEntry.Margin = SSIDs.Margin;
            passkeyEntry.FontSize = System.Windows.SystemParameters.PrimaryScreenWidth / 17;
            
        }

        private void setAnimationUp()
        {
            loadingAnimation = true;
            outerLoading.Opacity = 1;
            innerLoading.Opacity = 1;
        }

        private void setMessage2Up()
        {
            message2.Opacity = 0;
            message2.FontSize = System.Windows.SystemParameters.PrimaryScreenWidth / 50;
            message2.Text = "Where would you like to connect?";
            message2.Margin = new Thickness(0, this.Height/3.5, 0, 0);
        }

        private String state = "";

        private void setResultDimensions()
        {
            result.Height = outerLoading.Height;
            result.Width = outerLoading.Width;
            result.Margin = outerLoading.Margin;
        }

        private void setListViewDimensions()
        {
            SSIDs.Width = this.Width - 20;
            SSIDs.Height = this.Height / 3.5;
            SSIDs.Margin = new Thickness(0, cloudetteCloud.Height + cloudetteText.Height, 0, 0);
            SSIDs.Opacity = 0;
            strengthColumn.Width = SSIDs.Width / 2;
            accessPointNameColumn.Width = SSIDs.Width / 2;
        }

        private void setMessage1Up()
        {
            message1.Opacity = 1;
            message1.FontSize = System.Windows.SystemParameters.PrimaryScreenWidth / 44;
            message1.Margin = new Thickness(0, this.Height / 2, 0, 0);
        }


        private void setWindowDimensions()
        {
            this.Width = 400 * System.Windows.SystemParameters.PrimaryScreenWidth / 1280;
            this.Height = 600 * System.Windows.SystemParameters.PrimaryScreenHeight / 800;
        }

        private void setLogoDimensions()
        {
            cloudetteCloud.Height = 300 * System.Windows.SystemParameters.PrimaryScreenHeight / 2400;
            cloudetteCloud.Width = 212 * System.Windows.SystemParameters.PrimaryScreenWidth / 1600;
            
            cloudetteText.Height = 370 * System.Windows.SystemParameters.PrimaryScreenHeight / 2400;
            cloudetteText.Width = 272 * System.Windows.SystemParameters.PrimaryScreenWidth / 1600;
            cloudetteText.Margin = new Thickness(0, cloudetteText.Height/2 + cloudetteText.Height/50, 0, 0);
        }

        private bool checkForConnectionToCloudette()
        {
            outerLoading.Opacity = 1;
            innerLoading.Opacity = 1;
            if(CheckForConnection.checkConnectionToCloudette() == false)
            {
                outerLoading.Opacity = 0;
                innerLoading.Opacity = 0;
                result.Opacity = 1;
                leftButton.IsEnabled = true;
                rightButton.IsEnabled = true;
                leftButton.Opacity = 1;
                rightButton.Opacity = 1;
                loadingAnimation = false;
                state = "failed to connect";
                setUpButtons();
                message1.Text = "You aren't connected to a Cloudette!";
                
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool checkForInternetConnectivity()
        {
            if (CheckForConnection.checkConnectionToInternet() == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private ParseScanInformation psi = new ParseScanInformation();

        private async void startChecks()
        {
            
            await Task.Delay(2000);
            if (checkForConnectionToCloudette() == false)
            {
                return;
            }
            await Task.Delay(200);
            if (checkForInternetConnectivity() == false)
            {
                obtainSSIDData();
                await Task.Delay(7000);
                recv = recv.Replace("NEWLINE", "\n");
               
                psi.parseInformation(recv);
                SSIDs.Opacity = 1;
                message1.Opacity = 0;
                outerLoading.Opacity = 0;
                innerLoading.Opacity = 0;
                message2.Opacity = 1;
                populateListView(psi);
                leftButton.IsEnabled = true;
                rightButton.IsEnabled = true;
                loadingAnimation = false;
                leftButton.Opacity = 1;
                rightButton.Opacity = 1;
                state = "choose access point";
                setUpButtons();
            }
            else
            {
                await setUpSkyDriveCredentials();
            }
            
        }

        private void setUpButtons()
        {
            switch(state)
            {
                case ("choose access point"):
                leftButton.Content = "Search Again";
                rightButton.Content = "Confirm Selection";
                break;
                case ("failed to connect"):
                leftButton.Content = "Search Again";
                rightButton.Content = "Need help?";
                break;
                case ("enter passkey"):
                leftButton.Content = "Cancel and retry";
                rightButton.Content = "Confirm";
                break;
            }
           
        }

        private void populateListView(ParseScanInformation psi)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Access Point Name");
            dt.Columns.Add("Strength");
            

            for (int i = 0; i < psi.SSID.Count; i++)
            {
                dt.Rows.Add(psi.SSID[i], psi.Quality[i]);
            }
            SSIDs.ItemsSource = dt.DefaultView;
                    
        }

        private String recv = "";

        private async void obtainSSIDData()
        {
            Socket soc = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            System.Net.IPAddress ipAdd = System.Net.IPAddress.Parse("192.168.42.1");
            System.Net.IPEndPoint remoteEP = new System.Net.IPEndPoint(ipAdd, 6789);
            soc.Connect(remoteEP);

            byte[] byData = System.Text.Encoding.ASCII.GetBytes("SCAN\n");
            soc.Send(byData);
            await Task.Delay(4000);
            byte[] buffer = new byte[200000000];
            int iRx = soc.Receive(buffer);
            char[] chars = new char[iRx];
            System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
            int charLen = d.GetChars(buffer, 0, iRx, chars, 0);
            recv = new System.String(chars);
        }

        private Boolean loadingAnimation = true;

        private async void loadingAnimationOuter()
        {

            outerLoading.Height = 218 * System.Windows.SystemParameters.PrimaryScreenHeight / 1280;
            outerLoading.Width = 218 * System.Windows.SystemParameters.PrimaryScreenHeight / 800;
            outerLoading.Margin = new Thickness(0, cloudetteCloud.Height + cloudetteText.Height/2, 0, 0);
            int i = 0;
            while (loadingAnimation == true)
            {
                outerLoading.RenderTransform = new RotateTransform() { CenterX = outerLoading.Width / 2, CenterY = outerLoading.Height / 2, Angle = i };
                await Task.Delay(10);
                if (i == -360)
                {
                    i = 0;
                }
                i -= 2;
            }
        }  

        private async void loadingAnimationInner()
        {
            innerLoading.Height = 148 * System.Windows.SystemParameters.PrimaryScreenHeight / 1280;
            innerLoading.Width = 148 * System.Windows.SystemParameters.PrimaryScreenHeight / 800;
            innerLoading.Margin = new Thickness(0, cloudetteCloud.Height + cloudetteText.Height - cloudetteText.Height/3.1, 0, 0);
            int i = 0;
            while (loadingAnimation == true)
            {
                innerLoading.RenderTransform = new RotateTransform() { CenterX = innerLoading.Width / 2, CenterY = innerLoading.Height / 2, Angle = i };
                await Task.Delay(10);
                if(i == 360)
                {
                    i = 0;
                }
                i +=2;
            }
        }

        private void leftButton_Click(object sender, RoutedEventArgs e)
        {
            switch(state)
            {
                case("failed to connect"):
                    result.Opacity = 0;
                message1.Text = "Sorry about that! Let's try again...";
                setUpAndGo();
                leftButton.Opacity = 0;
                rightButton.Opacity = 0;
                break;
                case ("choose access point"):
                    result.Opacity = 0;
                setUpAndGo();
                psi.Quality.Clear();
                psi.SSID.Clear();
                message1.Text = "Sorry about that! Let's try again...";
                leftButton.Opacity = 0;
                rightButton.Opacity = 0;
                break;
                case ("enter passkey"):
                        result.Opacity = 0;
                setUpAndGo();
                message1.Text = "Sorry about that! Let's try again...";
                leftButton.Opacity = 0;
                rightButton.Opacity = 0;
                break;

            }
        }

        private int indexOfChoice;

        private void rightButton_Click(object sender, RoutedEventArgs e)
        {
            switch (state)
            {
                case ("failed to connect"):
                   //link to local HTML page giving help.
                    break;
                case ("choose access point"):
                    //ask for a password.               
                    indexOfChoice = SSIDs.SelectedIndex;
                    if (indexOfChoice >= 0)
                    {
                        message2.Text = "Great! Let's enter a passkey:";
                        state = "enter passkey";
                        SSIDs.Opacity = 0;
                        passkeyEntry.Visibility = System.Windows.Visibility.Visible;
                        passkeyEntry.Opacity = 1;
                        setUpButtons();
                    }
                    break;
                case ("enter passkey"):
                    //confirm and connect to Cloudette
                    String accessPointName = psi.SSID.ElementAt(indexOfChoice);
                    GenerateInterfacesFile gif = new GenerateInterfacesFile();
                    gif.generateInterfacesFiles(accessPointName, passkeyEntry.Password);
                    setUpSendingInterfacesUI();
                    sendInterfacesFile();
                    testForConnection();
                    break;
            }
        }

        public static Boolean skyDriveMarker = false;

        private async void testForConnection()
        {
            await Task.Delay(5000);
            for(int i = 0; i < 30; i ++)
            {
                if (CheckForConnection.checkConnectionToInternet() == true)
                {
                    await setUpSkyDriveCredentials();
                    break;
                }
                else if (i == 3)
                {
                    displayErrorMessage();
                }
                await Task.Delay(2500);
            }
        }

     
        private async Task setUpSkyDriveCredentials()
        {
            var skyDriveWindow = new SkyDriveLogin();
            skyDriveWindow.Show();

            while (true)
            {
                await Task.Delay(2700);
                if (skyDriveMarker == true)
                {
                    message1.Text = "You are successfully connected to Cloudette!";
                    await Task.Delay(2000);
                    this.Close();
                    Application.Current.Shutdown();
                }
            }
        }

        private void displayErrorMessage()
        {
            outerLoading.Opacity = 0;
            innerLoading.Opacity = 0;
            result.Opacity = 1;
            leftButton.IsEnabled = true;
            rightButton.IsEnabled = true;
            leftButton.Opacity = 1;
            rightButton.Opacity = 1;
            loadingAnimation = false;
            state = "failed to connect";
            setUpButtons();
            message1.Text = "Sorry! Looks like we couldn't connect you...Try again?";
        }

        //needs testing!
        private void setUpSendingInterfacesUI()
        {
            passkeyEntry.Opacity = 0;
            passkeyEntry.IsEnabled = false;
            message2.Opacity = 0;
            message1.Text = "Alright! Give us a moment to finish setting up...";
            message1.Opacity = 1;
            loadingAnimation = true;
            loadingAnimationInner();
            loadingAnimationOuter();
            setAnimationUp();
            SSIDs.Opacity = 0;
            SSIDs.IsEnabled = false;
            leftButton.Opacity = 0;
            rightButton.Opacity = 0;
            leftButton.IsEnabled = false;
            rightButton.IsEnabled = false;
        }

        private async void sendInterfacesFile()
        {
            Socket soc = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            System.Net.IPAddress ipAdd = System.Net.IPAddress.Parse("192.168.42.1");
            System.Net.IPEndPoint remoteEP = new System.Net.IPEndPoint(ipAdd, 6789);
            soc.Connect(remoteEP);
            await Task.Delay(2000);
            byte[] byData = System.Text.Encoding.ASCII.GetBytes("CHOSENSSID\n");
            soc.Send(byData);


            TcpClient tc = new TcpClient("192.168.42.1", 9876);
            NetworkStream ns = tc.GetStream();
            BinaryWriter bw = new BinaryWriter(ns);
            var bytes = File.ReadAllBytes("interfaces");
            bw.Write(bytes);
            
            bw.Flush();
            tc.Close();

        }

    }

   
}
