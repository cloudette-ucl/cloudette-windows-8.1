﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloudette
{
    class GenerateInterfacesFile
    {
        public void generateInterfacesFiles(String SSID, String passkey)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter("interfaces");
            file.WriteLine("auto lo");
            file.Write("{0}", Environment.NewLine);
            file.WriteLine("iface lo inet loopback");
            file.WriteLine("iface eth0 inet dhcp");
            file.Write("{0}", Environment.NewLine);
            file.Write("allow-hotplug wlan0");
            file.Write("{0}", Environment.NewLine);
            file.Write("{0}", Environment.NewLine);
            file.Write("iface wlan0 inet static");
            file.Write("{0}", Environment.NewLine);
            file.Write("\taddress 192.168.42.1");
            file.Write("{0}", Environment.NewLine);
            file.Write("\tnetmask 255.255.255.0");
            file.Write("{0}", Environment.NewLine);
            file.Write("{0}", Environment.NewLine);
            file.Write("allow-hotplug wlan1");
            file.Write("{0}", Environment.NewLine);
            file.Write("auto wlan1");
            file.Write("{0}", Environment.NewLine);
            file.Write("{0}", Environment.NewLine);
            file.Write("iface wlan1 inet dhcp");
            file.Write("{0}", Environment.NewLine);
            file.Write("\twpa-ssid \"" + SSID + "\"");
            file.Write("{0}", Environment.NewLine);
            file.Write("\twpa-psk \"" + passkey + "\"");
            file.Write("{0}", Environment.NewLine);
            file.Write("{0}", Environment.NewLine);
            file.Write("up iptables-restore < /etc/iptables.ipv4.nat");
            file.Close();
        }
    }
}
