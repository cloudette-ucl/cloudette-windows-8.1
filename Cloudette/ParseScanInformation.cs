﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloudette
{
    class ParseScanInformation
    {
        public List<String> Quality = new List<String>();
        public List<String> SSID = new List<String>();

        int numberOfAccessPoints = 0;

        public void parseInformation(String input)
        {
            numberOfAccessPoints = 1;
            getNumberOfCells(input);
            getQuality(input);
            getSSIDs(input);
            removeCloudette();
        }

        private void removeCloudette()
        {
            for(int i = 0; i < SSID.Count; i ++)
            {
                if (SSID[i].Equals("CLOUDETTE"))
                {
                    SSID.RemoveAt(i);
                    Quality.RemoveAt(i);
                    break;
                }
            }
        }

        private void getQuality(String input)
        {
            StringBuilder quality = new StringBuilder();
		    for (int k = 0; k < numberOfAccessPoints-1; k ++)
		    {
			    int i = input.IndexOf("Quality");
			    for(int j = i; j < input.Length; j ++)
			    {
				    if (input[j] == ' ')
				    {
					    input = input.Substring(j, input.Length-j);
					    Quality.Add(quality.ToString());
					    quality.Remove(0, quality.Length);
					    break;
				    }
				    else
				    {
					    quality.Append(input[j]);
				    }
			    }
		    }
		
		    for(int l = 0; l < Quality.Count; l ++)
		    {
                String str = Quality.ElementAt(l);
			    str = str.Replace("Quality=", "");
			    str = str.Replace("/70", "");
                Quality[l] = str;
		    }
        }

        private void getNumberOfCells(String input)
        {
            while (true)
            {
               
                if (input.Contains("Cell 0"+numberOfAccessPoints.ToString()))
                {
                    numberOfAccessPoints++;
                }
                else if (input.Contains("Cell " + numberOfAccessPoints.ToString()))
                {
                    numberOfAccessPoints++;
                }
                else
                {
                    break;
                }
            }
        }

        private void getSSIDs(String input)
        {
            StringBuilder ssid = new StringBuilder();
            for (int k = 0; k < numberOfAccessPoints - 1; k++)
            {
                int i = input.IndexOf("ESSID");
                for (int j = i; j < input.Length; j++)
                {
                    
                    if (input[j] == '\n')
                    {
                        input = input.Substring(j, input.Length-j);
                        ssid = ssid.Remove(ssid.Length-1, 1);
                        SSID.Add(ssid.ToString());
                        ssid.Remove(0, ssid.Length);
                        break;
                    }
                    else
                    {
                        ssid.Append(input[j]);
                    }
                }
            }

            for (int l = 0; l < SSID.Count; l++)
            {
                String str = SSID[l];
                str = str.Replace("ESSID:", "");
                str = str.Substring(1, str.Length - 1);
                SSID[l] = str;
            }
        }

    }
}
